#include "poker.hpp"

void WinTieEvaluation::setPlayerID(godot::String id) { this->PlayerID = id; }

void WinTieEvaluation::setWin(float value) { this->Win = value; }

void WinTieEvaluation::setTie(float value) { this->Tie = value; }

godot::String WinTieEvaluation::getPlayerID() { return this->PlayerID; }

float WinTieEvaluation::getWin() { return this->Win; }

float WinTieEvaluation::getTie() { return this->Tie; }

void WinTieEvaluation::_bind_methods() {
  // setters
  godot::ClassDB::bind_method(godot::D_METHOD("set_player_id", "id"),
                              &WinTieEvaluation::setPlayerID);
  godot::ClassDB::bind_method(godot::D_METHOD("set_win_value", "value"),
                              &WinTieEvaluation::setWin);
  godot::ClassDB::bind_method(godot::D_METHOD("set_tie_value", "value"),
                              &WinTieEvaluation::setTie);
  // getters
  godot::ClassDB::bind_method(godot::D_METHOD("get_player_id"),
                              &WinTieEvaluation::getPlayerID);
  godot::ClassDB::bind_method(godot::D_METHOD("get_win_value"),
                              &WinTieEvaluation::getWin);
  godot::ClassDB::bind_method(godot::D_METHOD("get_tie_value"),
                              &WinTieEvaluation::getTie);

  // properties
  godot::ClassDB::add_property(
      "WinTieEvaluation",
      godot::PropertyInfo(godot::Variant::STRING, "player_id"), "set_player_id",
      "get_player_id");
  godot::ClassDB::add_property(
      "WinTieEvaluation",
      godot::PropertyInfo(godot::Variant::FLOAT, "win_value"), "set_win_value",
      "get_win_value");
  godot::ClassDB::add_property(
      "WinTieEvaluation",
      godot::PropertyInfo(godot::Variant::STRING, "tie_value"), "set_tie_value",
      "get_tie_value");
}
