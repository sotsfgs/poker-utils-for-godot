#include "poker.hpp"

void PlayerHand::setPlayerID(godot::String id) { this->PlayerID = id; }
void PlayerHand::setHand(godot::PackedStringArray cards) {
  this->Cards = cards;
}

godot::String PlayerHand::getPlayerID() { return this->PlayerID; }
godot::PackedStringArray PlayerHand::getHand() { return this->Cards; }

void PlayerHand::_bind_methods() {
  // setters
  godot::ClassDB::bind_method(godot::D_METHOD("set_player_id", "id"),
                              &PlayerHand::setPlayerID);
  godot::ClassDB::bind_method(godot::D_METHOD("set_hand", "cards"),
                              &PlayerHand::setHand);
  // getters
  godot::ClassDB::bind_method(godot::D_METHOD("get_player_id"),
                              &PlayerHand::getPlayerID);
  godot::ClassDB::bind_method(godot::D_METHOD("get_hand"),
                              &PlayerHand::getHand);

  godot::ClassDB::add_property(
      "PlayerHand", godot::PropertyInfo(godot::Variant::STRING, "player_id"),
      "set_player_id", "get_player_id");
  godot::ClassDB::add_property(
      "PlayerHand",
      godot::PropertyInfo(godot::Variant::PACKED_STRING_ARRAY, "cards"),
      "set_hand", "get_hand");
}
