#ifndef POKER_H_
#define POKER_H_

#include "../godot-cpp/gdextension/gdextension_interface.h"
#include "../godot-cpp/include/godot_cpp/classes/ref.hpp"
#include "../godot-cpp/include/godot_cpp/core/class_db.hpp"
#include "../godot-cpp/include/godot_cpp/core/defs.hpp"
#include "../godot-cpp/include/godot_cpp/godot.hpp"
#include "../godot-cpp/include/godot_cpp/templates/vector.hpp"

struct PlayerHand : public godot::RefCounted {
  GDCLASS(PlayerHand, godot::RefCounted)

private:
  godot::String PlayerID;
  godot::PackedStringArray Cards;

public:
  void setPlayerID(godot::String id);
  void setHand(godot::PackedStringArray cards);
  godot::String getPlayerID();
  godot::PackedStringArray getHand();

protected:
  static void _bind_methods();
};

// PlayerValue holds any INT value related to a player's hand. This value can
// express anything, from card combination to hand rank
struct PlayerValue : public godot::RefCounted {
  GDCLASS(PlayerValue, godot::RefCounted);

private:
  godot::String PlayerID;
  float Value;

protected:
  static void _bind_methods();

public:
  void setPlayerID(godot::String id);
  void setValue(float rank);
  godot::String getPlayerID();
  float getValue();
};

struct WinTieEvaluation : public godot::RefCounted {
  GDCLASS(WinTieEvaluation, godot::RefCounted)

private:
  godot::String PlayerID;
  float Win;
  float Tie;

protected:
  static void _bind_methods();

public:
  // setters
  void setPlayerID(godot::String id);
  void setWin(float value);
  void setTie(float value);

  // getters
  godot::String getPlayerID();
  float getWin();
  float getTie();
};

struct HandsRankingEvaluationResult : public godot::RefCounted {
  GDCLASS(HandsRankingEvaluationResult, godot::RefCounted);

private:
  godot::TypedArray<PlayerValue> HandsRanking;
  godot::String Error;

protected:
  static void _bind_methods();

public:
  void setHands(godot::TypedArray<PlayerValue> rankings);
  void setError(godot::String error);
  godot::TypedArray<PlayerValue> getHands();
  godot::String getError();
};

struct SingleHandRankingEvaluationResult : public godot::RefCounted {
  GDCLASS(SingleHandRankingEvaluationResult, godot::RefCounted);

private:
  int HandRanking;
  godot::String Error;

protected:
  static void _bind_methods();

public:
  void setHandRanking(int ranking);
  int getHandRanking();
  void setError(godot::String error);
  godot::String getError();
};

class Poker : public godot::Object {
  GDCLASS(Poker, godot::Object);

protected:
  static void _bind_methods();

public:
  static godot::PackedStringArray getDeck();

  static godot::PackedStringArray getDeckShuffled();

  static godot::Ref<SingleHandRankingEvaluationResult>
  singleHandRankEvaluation(godot::PackedStringArray cards);

  static godot::Ref<HandsRankingEvaluationResult>
  handsRankingEvaluation(godot::PackedStringArray board,
                         godot::TypedArray<PlayerHand> players);

  static godot::TypedArray<PlayerValue>
  calculateEquities(godot::PackedStringArray board,
                    godot::PackedStringArray muck,
                    godot::TypedArray<PlayerHand> players);

  static godot::TypedArray<WinTieEvaluation>
  calculateWinTieFromEquities(godot::TypedArray<PlayerValue> equities,
                              int communityCards);
};

#endif // POKER_H_
