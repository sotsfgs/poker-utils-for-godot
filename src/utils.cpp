#include "utils.hpp"

std::vector<std::string> to_vecstr(godot::PackedStringArray input) {
  std::vector<std::string> result;

  for (const auto &item : input) {
    result.push_back(godot2stdstr(item));
  }

  return result;
};
