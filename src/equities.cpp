#include "../poker-utils/src/hand-evaluation.hpp"
#include "poker.hpp"
#include "utils.hpp"

godot::TypedArray<PlayerValue>
Poker::calculateEquities(godot::PackedStringArray board,
                         godot::PackedStringArray muck,
                         godot::TypedArray<PlayerHand> players) {
  auto gBoard = to_vecstr(board);
  auto gMuck = to_vecstr(muck);
  std::vector<poker::PlayerHand> gPlayers;

  for (auto index = 0; index < players.size(); index++) {
    auto player = godot::RefCounted::cast_to<PlayerHand>(players[index]);

    gPlayers.push_back(poker::PlayerHand{godot2stdstr(player->getPlayerID()),
                                         to_vecstr(player->getHand())});
  }

  auto equities = poker::calculateEquities(gBoard, gMuck, gPlayers);

  godot::TypedArray<PlayerValue> results;
  for (auto index = 0; index < equities.size(); index++) {
    auto equity = equities[index];

    godot::Ref<PlayerValue> player;
    player.instantiate();

    player->setValue(equity.Value);
    player->setPlayerID(equity.PlayerID.c_str());

    results.append(player);
  }

  return results;
}

godot::TypedArray<WinTieEvaluation>
calculateWinTieFromEquities(godot::TypedArray<PlayerValue> equities,
                            int communityCards) {

  std::vector<poker::HandEquityEvaluation> gEquities;
  for (auto index = 0; index < equities.size(); index++) {
    auto player = godot::RefCounted::cast_to<PlayerValue>(equities[index]);

    gEquities.push_back(
        {godot2stdstr(player->getPlayerID()), player->getValue()});
  }

  auto winties = poker::calculateWinTieFromEquities(gEquities, communityCards);
  godot::TypedArray<WinTieEvaluation> results;

  for (const auto wintie : winties) {
    godot::Ref<WinTieEvaluation> player;
    player.instantiate();

    player->setPlayerID(wintie.PlayerID.c_str());
    player->setWin(wintie.Win);
    player->setTie(wintie.Tie);

    results.push_back(player);
  }

  return results;
}
