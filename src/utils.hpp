#ifndef UTILS_H_
#define UTILS_H_

#include "../godot-cpp/gdextension/gdextension_interface.h"
#include "../godot-cpp/include/godot_cpp/classes/ref.hpp"
#include "../godot-cpp/include/godot_cpp/core/class_db.hpp"
#include "../godot-cpp/include/godot_cpp/core/defs.hpp"
#include "../godot-cpp/include/godot_cpp/godot.hpp"
#include "../godot-cpp/include/godot_cpp/templates/vector.hpp"

#include <string>
#include <vector>

// From
// https://ask.godotengine.org/18552/gdnative-convert-godot-string-to-const-char?show=155549#c155549
inline std::string godot2stdstr(const godot::String &s) {
  return std::string((const char *)s.to_utf8_buffer().ptr());
}

std::vector<std::string> to_vecstr(godot::PackedStringArray input);

#endif // UTILS_H_
