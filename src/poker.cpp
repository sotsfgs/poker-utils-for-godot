#include "poker.hpp"
#include "../poker-utils/src/deck.hpp"
#include "../poker-utils/src/hand-evaluation.hpp"
#include "utils.hpp"
#include <string>
#include <vector>

void Poker::_bind_methods() {
  godot::ClassDB::bind_static_method("Poker", godot::D_METHOD("get_deck"),
                                     &Poker::getDeck);
  godot::ClassDB::bind_static_method(
      "Poker", godot::D_METHOD("get_deck_shuffled"), &Poker::getDeckShuffled);
  godot::ClassDB::bind_static_method("Poker",
                                     godot::D_METHOD("hand_ranking", "cards"),
                                     &Poker::singleHandRankEvaluation);
  godot::ClassDB::bind_static_method(
      "Poker", godot::D_METHOD("hands_ranking_evaluation", "board", "players"),
      &Poker::handsRankingEvaluation);
  godot::ClassDB::bind_static_method(
      "Poker",
      godot::D_METHOD("calculate_equities", "board", "muck", "players"),
      &Poker::calculateEquities);
}

godot::PackedStringArray Poker::getDeck() {
  godot::PackedStringArray cards;
  auto deck = poker::getDeck();

  for (const auto &card : deck) {
    cards.push_back(godot::String(card.c_str()));
  }

  return cards;
}

godot::PackedStringArray Poker::getDeckShuffled() {
  godot::PackedStringArray cards;
  auto deck = poker::getShuffledDeck();

  for (const auto &card : deck) {
    cards.push_back(godot::String(card.c_str()));
  }

  return cards;
}

godot::Ref<SingleHandRankingEvaluationResult>
Poker::singleHandRankEvaluation(godot::PackedStringArray cards) {
  poker::Cards all_cards;

  for (const auto &card : cards) {
    all_cards.push_back(std::string(godot2stdstr(card)));
  }

  auto result = poker::singleHandRankEvaluation(all_cards);
  if (std::holds_alternative<poker::Error>(result)) {
    godot::Ref<SingleHandRankingEvaluationResult> evaluation;
    evaluation.instantiate();

    evaluation->setError(std::get<poker::Error>(result).message.c_str());
    return evaluation;
  }

  godot::Ref<SingleHandRankingEvaluationResult> evaluation;
  evaluation.instantiate();

  int ranking = std::get<int>(result);
  evaluation->setHandRanking(ranking);

  return evaluation;
};

godot::Ref<HandsRankingEvaluationResult>
Poker::handsRankingEvaluation(godot::PackedStringArray board,
                              godot::TypedArray<PlayerHand> players) {
  std::vector<std::string> boardStr = to_vecstr(board);
  std::vector<poker::PlayerHand> playersStr;

  for (auto index = 0; index < players.size(); index++) {
    auto player = godot::RefCounted::cast_to<PlayerHand>(players[index]);

    playersStr.push_back(poker::PlayerHand{godot2stdstr(player->getPlayerID()),
                                           to_vecstr(player->getHand())});
  }

  godot::Ref<HandsRankingEvaluationResult> result;
  result.instantiate();

  auto evaluationResults = poker::handsRankingEvaluation(boardStr, playersStr);
  if (std::holds_alternative<poker::Error>(evaluationResults)) {
    auto error = std::get<poker::Error>(evaluationResults);

    result->setError(godot::String(error.message.c_str()));

    return result;
  }

  auto evaluations =
      std::get<poker::HandsRankingEvaluations>(evaluationResults);
  godot::TypedArray<PlayerValue> rankings;
  for (const auto &evaluation : evaluations) {
    godot::Ref<PlayerValue> hand;
    hand.instantiate();

    hand->setPlayerID(godot::String(evaluation.PlayerID.c_str()));
    hand->setValue(evaluation.Value);

    rankings.push_back(hand);
  }

  result->setHands(rankings);

  return result;
}
