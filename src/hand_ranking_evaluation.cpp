#include "poker.hpp"

// SingleHandRankingEvaluation
void SingleHandRankingEvaluationResult::setHandRanking(int ranking) {
  this->HandRanking = ranking;
}

int SingleHandRankingEvaluationResult::getHandRanking() {
  return this->HandRanking;
}

void SingleHandRankingEvaluationResult::setError(godot::String error) {
  this->Error = error;
}

godot::String SingleHandRankingEvaluationResult::getError() {
  return this->Error;
};

void SingleHandRankingEvaluationResult::_bind_methods() {
  godot::ClassDB::bind_method(
      godot::D_METHOD("set_hand_ranking", "ranking"),
      &SingleHandRankingEvaluationResult::setHandRanking);
  godot::ClassDB::bind_method(
      godot::D_METHOD("get_hand_ranking"),
      &SingleHandRankingEvaluationResult::getHandRanking);
  godot::ClassDB::bind_method(godot::D_METHOD("set_error", "error"),
                              &SingleHandRankingEvaluationResult::setError);
  godot::ClassDB::bind_method(godot::D_METHOD("get_error"),
                              &SingleHandRankingEvaluationResult::getError);

  godot::ClassDB::add_property(
      "SingleHandRankingEvaluationResult",
      godot::PropertyInfo(godot::Variant::INT, "hand_ranking"),
      "set_hand_ranking", "get_hand_ranking");
  godot::ClassDB::add_property(
      "SingleHandRankingEvaluationResult",
      godot::PropertyInfo(godot::Variant::STRING, "error"), "set_error",
      "get_error");
}

// HandsRankingEvaluation
void HandsRankingEvaluationResult::setHands(
    godot::TypedArray<PlayerValue> rankings) {
  this->HandsRanking = rankings;
}

void HandsRankingEvaluationResult::setError(godot::String error) {
  this->Error = error;
}

godot::String HandsRankingEvaluationResult::getError() { return this->Error; }

godot::TypedArray<PlayerValue> HandsRankingEvaluationResult::getHands() {
  return this->HandsRanking;
};

void HandsRankingEvaluationResult::_bind_methods() {
  // setters
  godot::ClassDB::bind_method(godot::D_METHOD("set_hands", "rankings"),
                              &HandsRankingEvaluationResult::setHands);
  godot::ClassDB::bind_method(godot::D_METHOD("set_error", "error"),
                              &HandsRankingEvaluationResult::setError);
  // getters
  godot::ClassDB::bind_method(godot::D_METHOD("get_hands"),
                              &HandsRankingEvaluationResult::getHands);
  godot::ClassDB::bind_method(godot::D_METHOD("get_error"),
                              &HandsRankingEvaluationResult::getError);

  godot::ClassDB::add_property(
      "HandsRankingEvaluationResult",
      godot::PropertyInfo(godot::Variant::INT, "hands_ranking"), "set_hands",
      "get_hands");
  godot::ClassDB::add_property(
      "HandsRankingEvaluationResult",
      godot::PropertyInfo(godot::Variant::STRING, "error"), "set_error",
      "get_error");
}

// PlayerValue
void PlayerValue::setPlayerID(godot::String id) { this->PlayerID = id; }
void PlayerValue::setValue(float rank) { this->Value = rank; }
godot::String PlayerValue::getPlayerID() { return this->PlayerID; }
float PlayerValue::getValue() { return this->Value; }
void PlayerValue::_bind_methods() {
  // setters
  godot::ClassDB::bind_method(godot::D_METHOD("set_player_id", "id"),
                              &PlayerValue::setPlayerID);
  godot::ClassDB::bind_method(godot::D_METHOD("set_hand_rank", "rank"),
                              &PlayerValue::setValue);
  // getters
  godot::ClassDB::bind_method(godot::D_METHOD("get_player_id"),
                              &PlayerValue::getPlayerID);
  godot::ClassDB::bind_method(godot::D_METHOD("get_hand_rank"),
                              &PlayerValue::getValue);

  godot::ClassDB::add_property(
      "PlayerValue",
      godot::PropertyInfo(godot::Variant::STRING, "player_id"), "set_player_id",
      "get_player_id");
  godot::ClassDB::add_property(
      "PlayerValue", godot::PropertyInfo(godot::Variant::FLOAT, "value"),
      "set_hand_rank", "get_hand_rank");
}
