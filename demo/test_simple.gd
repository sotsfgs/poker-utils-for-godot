extends GutTest

func test_get_deck():
	var deck = Poker.get_deck()
	var card = deck[0]
	deck.remove_at(0)
	assert_eq(card, "2s")

func test_get_deck_shuffled():
	var deck1 = Poker.get_deck_shuffled()
	var deck2 = Poker.get_deck_shuffled()
	assert_ne(deck1, deck2)
