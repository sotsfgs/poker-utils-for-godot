#!/usr/bin/env python3
import os

libs = []

for root, dirs, files in os.walk("extension-install/godot-poker-utils/lib"):
    for file in files:
        if file.startswith("libgodot-poker-utils.") and ".dll." not in file:
            libs.append(file)

libs.sort()

print(
    """[configuration]

entry_symbol = \"poker_utils_library_init\"
compatibility_minimum = \"4.1\"

[libraries]"""
)

lib_path = "res://addons/godot-poker-utils/lib"

for lib in libs:
    parts = lib.split(".")
    os = parts[1]
    arch = parts[2]
    is_debug = arch.endswith("-d")
    build_type = "release"
    if is_debug:
        arch = arch[:-2]
        build_type = "debug"

    print(f'{os}.{build_type}.{arch} = "{lib_path}/{lib}"')
