# Poker utilities for Godot
An extension written in C++ for Godot 4 that handles complex matters for poker games.
## Why does this project exists
Because I want to make a Texas Hold'em game with Godot 4, but I don't know the nitty gritty about evaluating hands of cards thus I offloaded the job on [`poker-utils`](https://gitlab.com/sotsfgs/poker-utils) C++'s library (which in turn uses [OMPEval](https://gitlab.com/sotsfgs/ompeval) underneath). We also offload the work of shuffling the cards with seeds of 256 bits ([226 bits required to shuffle 52 cards](https://stackoverflow.com/questions/57332878/can-fisher-yates-shuffle-produce-all-playing-card-permutations)) as Godot 4 [`randi()` 2^32-1 bits](https://docs.godotengine.org/en/stable/tutorials/math/random_number_generation.html#getting-a-random-number) falls short.
## To Do
- [X] Hand evaluation for the board and *N* players.
- [X] Calculate hand equity and the Win/Tie for each player hand.
- [X] 52 cards deck shuffled.
- [X] Bind types and functions from the source code to GDScript with GDExtension
- [ ] Compile to major OS (Windows, GNU/Linux, MacOS) and phone platforms (Android, iOS)
  - [X] GNU/Linux
  - [X] Windows
  - [ ] MacOS
  - [ ] iOS
  - [X] Android
- [ ] Add documentation
