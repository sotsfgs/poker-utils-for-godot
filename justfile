package: build-all
    ./package-addon.sh

build:
    ./build.sh

build-compile-commands: build

build-windows:
    CMAKE_TOOLCHAIN_FILE=./toolchains/TC-mingw.cmake ./build.sh windows

build-android:
    CMAKE_TOOLCHAIN_FILE=$TOOLCHAIN_FILE ./build-android.sh

build-all: build build-windows build-android
    ./gen-manifest.py > extension-install/godot-poker-utils/godot-poker-utils.gdextension

install-on-demo: build-all
    cp -r extension-install/godot-poker-utils demo/addons

install-on-demo-nix: build build-android
    ./gen-manifest.py > extension-install/godot-poker-utils/godot-poker-utils.gdextension
    cp -r extension-install/godot-poker-utils demo/addons

test:
    godot4 --headless -d -s --path "demo/" addons/gut/gut_cmdln.gd -gexit
