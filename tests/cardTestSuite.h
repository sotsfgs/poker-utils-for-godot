#include "../src/hand-evaluation.hpp"
#include "cxxtest/TestSuite.h"

using namespace poker;

class CardValidationTestSuite : public CxxTest::TestSuite {
public:
  void testGetCardIndex(void) {
    struct testElement {
      int expectedIndex;
      std::string card;
    };

    std::vector<testElement> testTable = {
        testElement{0, "2s"}, testElement{1, "2h"}, testElement{2, "2c"},
        testElement{3, "2d"}, testElement{4, "3s"}, testElement{5, "3h"},
        testElement{6, "3c"}, testElement{7, "3d"}, testElement{8, "4s"},
        testElement{9, "4h"}, testElement{10, "4c"}, testElement{11, "4d"},
        // ...
        testElement{48, "As"}, testElement{49, "Ah"}, testElement{50, "Ac"},
        testElement{51, "Ad"}};

    for (const auto &element : testTable) {
      auto result = getCardIndex(element.card);

      if (std::holds_alternative<Error>(result)) {
        TS_FAIL(std::get<Error>(result).message + ": " + element.card);
      } else {
        TS_ASSERT_EQUALS(element.expectedIndex, std::get<int>(result))
      }
    };
  };
};
