#include "../src/hand-evaluation.hpp"
#include <cxxtest/TestSuite.h>

using namespace poker;

class HandValidationTestSuite : public CxxTest::TestSuite {
public:
  void testHandRanking(void) {
    struct testElement {
      std::vector<std::string> hand;
      std::string rank;
      int rankIndex;
    };

    std::vector<testElement> testTable = {
        testElement{{"As", "Qs", "Ts", "Js", "Ks"}, "Royal flush", 10},
        testElement{{"2s", "3s", "4s", "5s", "6s"}, "Straight flush", 9},
        testElement{{"2s", "2h", "2c", "2d", "6s"}, "Four of a kind", 8},
        testElement{{"Th", "Td", "Ts", "9c", "9d"}, "Full house", 7},
        testElement{{"2s", "Ks", "9s", "4s", "Ts"}, "Flush", 6},
        testElement{{"9s", "8d", "7c", "6h", "5h"}, "Straight", 5},
        testElement{{"3s", "3h", "3d", "Ks", "Qd"}, "Three of a kind", 4},
        testElement{{"Ah", "Ad", "Kh", "Kd", "Qs"}, "Two pair", 3},
        testElement{{"2s", "2h", "9s", "8d", "7c"}, "Pair", 2},
        testElement{{"Ah", "Kd", "Js", "8c", "3s"}, "High card", 1},
    };

    for (const auto &element : testTable) {
      auto result = singleHandRankEvaluation(element.hand);

      if (std::holds_alternative<Error>(result)) {
        TS_FAIL(std::get<Error>(result).message);
      } else {
        int value = std::get<int>(result);
        // test the hand ranking name
        TS_ASSERT_EQUALS(element.rank, getHandName(value));
        // test the hand ranking position
        TS_ASSERT_EQUALS(element.rankIndex, getHandRank(value));
      }
    };
  }

  void testHandValidationTie(void) {
    Cards board = {"7s", "8s", "9s", "Ts", "Js"};
    Cards deadCards = {"3s", "4d"};
    std::vector<PlayerHand> players = {{"Player1", {"Qd", "Kc"}},
                                       {"Player2", {"Ac", "2c"}},
                                       {"Player3", {"3h", "4h"}}};

    auto equities = calculateEquities(board, deadCards, players);
    auto result = calculateWinTieFromEquities(equities, board.size());
    TS_ASSERT_EQUALS(result[0].Win, 0.0);
    TS_ASSERT_EQUALS(result[0].Tie, 1.0);
    // player 2
    TS_ASSERT_EQUALS(result[1].Win, 0.0);
    TS_ASSERT_EQUALS(result[1].Tie, 1.0);
    // player 3
    TS_ASSERT_EQUALS(result[2].Win, 0.0);
    TS_ASSERT_EQUALS(result[2].Tie, 1.0);
  };

  void testHandValidationPlayer3Wins(void) {
    Cards board = {"5s", "7s", "Jd", "Kc", "2s"};
    Cards deadCards = {"3s", "4h"};
    std::vector<PlayerHand> players = {{"Player1", {"Qs", "Kh"}},
                                       {"Player2", {"Js", "Jh"}},
                                       {"Player3", {"4s", "6s"}}};

    auto equities = calculateEquities(board, deadCards, players);
    auto result = calculateWinTieFromEquities(equities, board.size());

    TS_ASSERT_EQUALS(result[0].PlayerID, "Player3");
    TS_ASSERT_EQUALS(result[0].Win, 1.0);
    TS_ASSERT_EQUALS(result[0].Tie, 0.0);

    TS_ASSERT_EQUALS(result[1].PlayerID, "Player1");
    TS_ASSERT_EQUALS(result[1].Win, 0.0);
    TS_ASSERT_EQUALS(result[1].Tie, 0.0);

    TS_ASSERT_EQUALS(result[2].PlayerID, "Player2");
    TS_ASSERT_EQUALS(result[2].Win, 0.0);
    TS_ASSERT_EQUALS(result[2].Tie, 0.0);
  };

  void testHandValidationPlayer2closeToWin(void) {
    Cards board = {"5s", "7s", "Jd"};
    Cards deadCards = {};
    std::vector<PlayerHand> players = {{"Player1", {"Qs", "Kh"}},
                                       {"Player2", {"Js", "Jh"}},
                                       {"Player3", {"4s", "6s"}}};

    auto equities = calculateEquities(board, deadCards, players);
    auto result = calculateWinTieFromEquities(equities, board.size());

    // best hand, three of a kind
    TS_ASSERT_EQUALS(result[0].PlayerID, "Player2");

    // second best hand, straight
    TS_ASSERT_EQUALS(result[1].PlayerID, "Player3");

    TS_ASSERT_EQUALS(result[2].PlayerID, "Player1");
  }
};
