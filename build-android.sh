#!/usr/bin/env sh

set -e

for build_type in Release Debug
do
  rm -rf extension-build-android
  cmake \
    -B "extension-build-android" \
    -G "Ninja" \
    -DCMAKE_BUILD_TYPE=$build_type \
    -DANDROID_ABI=$ABI \
    -DANDROID_PLATFORM=android-$MINSDKVERSION \
    --install-prefix "$(pwd)/extension-install" \
    .

  cmake --build "extension-build-android"

  cmake --install "extension-build-android"
done
