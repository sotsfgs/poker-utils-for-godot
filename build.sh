#!/usr/bin/env sh

set -e

optional_param=""

if [ -n "$1" ]; then
    optional_param="-$1"
fi


for build_type in Release Debug
do
  cmake \
    -B "extension-build${optional_param}" \
    -G "Ninja" \
    -DCMAKE_BUILD_TYPE=$build_type \
    --install-prefix "$(pwd)/extension-install" \
    .

  cmake --build "extension-build${optional_param}"

  cmake --install "extension-build${optional_param}"
done
